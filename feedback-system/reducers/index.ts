import {combineReducers} from 'redux';
import {mockReducer} from '../modules/albums/controllers/services/reducers';
import {reducer as feedbackReducer} from '../modules/statistic';

const rootReducer = combineReducers({
    mockReducer,
    feedbackReducer
});

export default rootReducer;
