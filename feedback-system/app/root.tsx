import React, {Component} from 'react';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from '../store';
import App from './app';

interface IProps {
    // TODO: add right type for history
    history: any;
}

class Root extends Component<IProps> {
    render() {
        return (
            <Provider store={store}>
                <Router history={this.props.history}>
                    <App/>
                </Router>
            </Provider>
        );
    }
}

export default Root;
