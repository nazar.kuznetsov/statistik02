import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga/root';
import rootReducer from '../reducers';
import apiMiddleware from 'feedback-system/middleware/api';

import {IState} from './typedef';

// TODO: set normal type
declare var window: {
    __REDUX_DEVTOOLS_EXTENSION__?: any;
};

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(preloadedState: IState) {
    const store = createStore(
        rootReducer,
        preloadedState,
        compose(
            applyMiddleware(
                apiMiddleware,
                sagaMiddleware
            ),
            window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f: any) => f)
    );

    sagaMiddleware.run(rootSaga);

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
