import {INTERNAL_ERROR} from './constants';
import api from './middleware';
import {request} from './request';

const handleErrors = (({responseData, response: {status, ok}}) => {
    if (!ok) {
        return Promise.reject(responseData);
    }

    return responseData;
});

const callApi = (callInfo, store) => {
    const API_ROOT = '/';

    return request(callInfo, API_ROOT)
        .then(({responseData, response: {status, ok}}) => {
            return handleErrors({responseData, response: {status, ok}, store, callInfo});
        }).catch(error => {
            return Promise.reject(error);
        });
};

export default api(INTERNAL_ERROR, callApi);
