// type IValidateAction = ({types: [any, any, any], endpoint: string}) => void;

// @ts-ignore
export const validateAction = ({types, endpoint}) => {
    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.');
    }
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.');
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('Expected action types to be strings.');
    }
};
