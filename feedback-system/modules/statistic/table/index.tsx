import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import {compose} from 'redux';
import Feedback from './feedback';
import Title from './title';
import {
  deleteFeedback,
  sortAscFeedback,
  sortDescFeedback
} from '../service/action';

interface IFeedback {
  nameTo: string;
  nameFrom: string;
  rating: number;
  comments: string;
  id: string;
}

interface ITheme {
  root: {
    width: string,
    marginTop: number,
    overflowX: string
  };
  table: {
    minWidth: number
  };
  spacing: {
    unit: number;
  };
}

interface IProps {
  classes: {
    root: string;
    table: string;
  };
  deleteFeedback: (id: string) => void;
}

interface IState {
  orderBy: string;
  data: IFeedback[];
  direction: 'asc' | 'desc';
}

/* Table title
================================================================================= */
const title = [
  { id: 'nameTo', label: 'От кого', numeric: false },
  { id: 'nameFrom', label: 'Кому', numeric: false },
  { id: 'rating', label: 'Оценка', numeric: false },
  { id: 'comments', label: 'Коментарий', numeric: false }
];

const styles = (theme: ITheme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
});

class SimpleTable extends Component<IProps, IState> {
  state = {
    orderBy: 'nameTo',
    direction: 'desc'
  };

  sorting = (type: string) => {
    // const clone = [...this.props.feedbackList];
    // const types = this.state.orderBy;
    // let sort;
    // if (this.state.direction === 'desc') {
    //   sort = type === 'string'
    //     ? (a: IFeedback, b: IFeedback) => a[types].localeCompare(b[types])
    //     : (a: IFeedback, b: IFeedback) => a[types] - b[types];

    // } else {
    //   sort = type === 'string'
    //     ? (a: IFeedback, b: IFeedback) => b[types].localeCompare(a[types])
    //     : (a: IFeedback, b: IFeedback) => b[types] - a[types];
    // }

    // const feedback = clone.sort(sort);
    // this.setState({ data: feedback });

  }

  derection = (name: string, type: 'string' | 'number') => {
    this.setState({
      orderBy: name},
      () => this.sorting(type)
    );
  }

  /* Sorting On Click Title
  ================================================================================= */
  handleClick = (id: string) => {
    const type = id === 'rating' ? 'number' : 'string';
    this.setState((state) => {
      let result = '';
      if (state.orderBy === id) {
        result = state.direction === 'desc' ? 'asc' : 'desc';
      } else {
        result = 'desc';
      }
      return {
        direction: result
      };
    }, () => this.derection(id, type));
  }

  /* Delete feedback
  ================================================================================= */
  delete = (id: string) => {
    this.props.deleteFeedback(id);
  }

  componentDidMount() {
    this.props.sortDescFeedback('string', 'nameTo');
  }

  render() {
    const { classes, feedbackList } = this.props;

    return (
      <Paper className={classes.root}>
        {
          feedbackList.length > 0
            ? <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  {
                    title.map(headline =>
                      <Title
                        key={headline.id}
                        label={headline.label}
                        id={headline.id}
                        direction={this.state.direction}
                        orderBy={this.state.orderBy}
                        handleClick={this.handleClick}
                      />
                    )}
                </TableRow>
              </TableHead>
              <TableBody>
                {feedbackList.map(row => (
                  <TableRow key={row.id}>
                    <Feedback
                      nameTo={row.nameTo}
                      nameFrom={row.nameFrom}
                      rating={row.rating}
                      id={row.id}
                      comments={row.comments}
                      onDelete={this.delete}
                    />
                  </TableRow>
                ))
                }
              </TableBody>
            </Table>
            : null
        }
      </Paper>
    );
  }
}

// export default withStyles(styles)(SimpleTable);

const mapStateToProps = ({feedbackReducer}) => {
  return {
    feedbackList: feedbackReducer.feedback
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteFeedback: (id: string) => dispatch(deleteFeedback(id)),
    sortAscFeedback: (type: string, value: string) => dispatch(sortAscFeedback(type, value)),
    sortDescFeedback: (type: string, value: string) => dispatch(sortDescFeedback(type, value))
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SimpleTable);
