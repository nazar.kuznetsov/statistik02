import React from 'react';
import { TableCell } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import RestoreIcon from '@material-ui/icons/Restore';

interface IFeedback {
    nameTo: string;
    nameFrom: string;
    rating: number;
    comments: string;
    id: string;
    onDelete: (id: string) => void;
}

const Feedback = ({ nameTo, nameFrom, id, rating, comments, onDelete }: IFeedback) => {

    const handleClick = () => {
        onDelete(id);
    };

    return (
        <>
            <TableCell>{nameTo}</TableCell>
            <TableCell>{nameFrom}</TableCell>
            <TableCell>{rating}</TableCell>
            <TableCell>{comments}</TableCell>

            <TableCell padding="checkbox">
                <RestoreIcon />
            </TableCell>

            <TableCell
                onClick={handleClick}
                padding="checkbox">
                <DeleteIcon />
            </TableCell>
        </>
    );
};

export default Feedback;
