import { api } from './api';
import { FEEDBACK_DELETE, FEEDBACK_SORT_DESC, FEEDBACK_SORT_ASC } from './constans';

const initialState = {
  feedback: api
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FEEDBACK_DELETE:
      return {
        ...state, feedback: state.feedback.filter(element => element.id !== action.payload.id)
      };
      case FEEDBACK_SORT_DESC:
      return {
        ...state, feedback:
        [...state.feedback].sort((a, b) => a[action.payload.value].localeCompare(b[action.payload.value]))
      };
      case FEEDBACK_SORT_ASC:
      return {
        ...state, feedback:
        [...state.feedback].sort((a, b) => b[action.payload.value].localeCompare(a[action.payload.value]))
      };
    default: return state;
  }
};
