import {
  FEEDBACK_DELETE,
  FEEDBACK_SORT_DESC,
  FEEDBACK_SORT_ASC
 } from './constans';

export const deleteFeedback = (id: string) => {
    return {
      type: FEEDBACK_DELETE,
      payload: {id}
    };
};

export const sortDescFeedback = (type: string, value: string) => {
  return {
    type: FEEDBACK_SORT_DESC,
    payload: {
      type,
      value
    }
  };
};

export const sortAscFeedback = (type: string, value: string) => {
  return {
    type: FEEDBACK_SORT_ASC,
    payload: {
      type,
      value
    }
  };
};
