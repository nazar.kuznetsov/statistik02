interface IFeedback {
  nameTo: string;
  nameFrom: string;
  rating: number;
  comments: string;
  id: string;
}

export const api: IFeedback[] = [
  {
    nameTo: 'Андрей  Кличко',
    id: '001',
    nameFrom: 'Тлекс Дрон',
    rating: 3,
    comments: 'Ок'
  },
  {
    nameTo: 'Петро  Порошенко',
    id: '002',
    rating: 2,
    nameFrom: 'Макс Опг',
    comments: 'Молодец'
  },
  {
    nameTo: 'Виктор  Янукович',
    id: '003',
    nameFrom: 'Алкоголик Черный',
    rating: 1,
    comments: 'Плохо'
  },
  {
    nameTo: 'Аладимир Путин',
    id: '004',
    nameFrom: 'Янукович Виктор',
    rating: 4,
    comments: 'Молодец'
  },
  {
    nameTo: 'Барак Обама',
    id: '005',
    rating: 8,
    nameFrom: 'Космос Олеп',
    comments: 'Класс'
  },
  {
    nameTo: 'Ким Чен Ир',
    id: '006',
    rating: 7,
    nameFrom: 'Владимир Кличко',
    comments: 'Круто'
  }
];
