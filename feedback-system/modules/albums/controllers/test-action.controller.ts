import {Component} from 'react';
import {connect} from 'react-redux';
import {mockedFetchAction, mockedAction} from './services/actions';

interface IDispatch {
    mockedAction: typeof mockedAction;
    mockedFetchAction: typeof mockedFetchAction;
}

type Props = IDispatch;

class TestActionController extends Component<Props> {
    componentDidMount() {
        // this.props.mockedFetchAction();
        this.props.mockedAction('mocked value');
    }

    render() {
        return null;
    }
}

const mapDispatchToProps: IDispatch = {mockedFetchAction, mockedAction};

const connector = connect(null, mapDispatchToProps);

export default connector(TestActionController);
