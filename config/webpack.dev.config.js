const webpackConfig = require('./webpack.common');
const webpackMerge = require('webpack-merge');

module.exports = webpackMerge(webpackConfig, {
    mode: 'development',
    devtool: process.env.SOURCE_MAP === 'true' ? 'source-map' : false
});
